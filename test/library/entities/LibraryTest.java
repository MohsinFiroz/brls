package library.entities;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;

import library.borrowbook.BorrowBookControl;
import library.entities.ILoan.LoanState;
import library.entities.helpers.BookHelper;
import library.entities.helpers.LoanHelper;
import library.entities.helpers.PatronHelper;

class LibraryTest {
	ILibrary library;
	ILoan loan;
	@Mock IBook mockBook;
	@Mock IPatron mockPatron;
	//ICalendar calendar;

	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		library = new Library(new BookHelper(), new PatronHelper(), new LoanHelper());
		when(mockPatron.canBorrow()).thenReturn(true);
		when(mockBook.isAvailable()).thenReturn(true);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testCalculateOverDueFine() {
		//arrange
		java.util.Calendar calendar = java.util.Calendar.getInstance();
		calendar.set(2020, 9, 19, 00, 00, 00);
		Date dueDate = calendar.getTime();
		double expected = 1.0;
		loan = new Loan(mockBook, mockPatron, dueDate, LoanState.OVER_DUE, 1);
		System.out.print("Due date:" + loan.getDueDate());
		
		//act
		double actual = library.calculateOverDueFine(loan);
		
		//assert
		assertEquals(expected, actual);
	}
	
	@Test
	void testCalculateOverDueFineWhenDueTwoDays() {
		//arrange
		java.util.Calendar calendar = java.util.Calendar.getInstance();
		calendar.set(2020, 9, 18, 00, 00, 00);
		Date dueDate = calendar.getTime();
		double expected = 2.0;
		loan = new Loan(mockBook, mockPatron, dueDate, LoanState.OVER_DUE, 1);
		System.out.print("Due date:" + loan.getDueDate());
		
		//act
		double actual = library.calculateOverDueFine(loan);
		
		//assert
		assertEquals(expected, actual);
	}


}
